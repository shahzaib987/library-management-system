import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.*;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

public class user_returnbook extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					user_returnbook frame = new user_returnbook();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public user_returnbook() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Documents and Settings\\Administrator\\Desktop\\bk pics\\java prjct\\icon.gif"));
		setTitle("Library Management System (Return Book) ");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 576, 328);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Student Name:");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setBounds(134, 72, 132, 14);
		contentPane.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(244, 69, 150, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("Return his/her Book(s) !");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Connection con=null;
				try {
				      Class.forName("sun.jdbc.odbc.JdbcOdbcDriver") ;

				// Connect with a full url string
				         con = DriverManager.getConnection("jdbc:odbc:Student");
				      //JOptionPane.showMessageDialog(null, "Database Connection Confirmed!");
				      	Statement stmt=con.createStatement();
				    	JOptionPane.showMessageDialog(null, "Successful , Thank you for returning Books !");
				      	ResultSet rs=stmt.executeQuery("UPDATE BookIssue SET BookReturned = 1 where Username1='" +textField.getText()+ "'");
				    	//ResultSet alr=stmt.executeQuery("select ReturnDate from user_inf where Username='" +StdLogin.textField.getText()+ "'");
				    	rs.next();
				      	con.close();
						
						   
			    } catch (Exception e1) {
			     
			    }
			
			  }
		
		
	});
		btnNewButton.setBounds(344, 143, 195, 23);
		contentPane.add(btnNewButton);
		
		JButton btnCheckStudentRecord = new JButton("Check Student Record Of Borrowed Books !");
		btnCheckStudentRecord.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Object[][] s=new Object[1][06];
				try
				{
				    Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			    	Connection cn=DriverManager.getConnection("jdbc:odbc:Student");
			    	Statement stmt=cn.createStatement();
			    	 JOptionPane.showMessageDialog(null,"Searching For Student Borrowed Book(s)........!");
			    	ResultSet rs=stmt.executeQuery("select *  from  BookIssue where Username1='" +textField.getText()+ "'");
			    	//ResultSetMetaData rsmd = rs.getMetaData();
			    	//rsmd.getColumnName(1);
			    	//rsmd.getColumnName(2);
			    	//rsmd.getColumnName(3);
			    	//rsmd.getColumnName(4);
			    	//rsmd.getColumnName(5);
			    	//rsmd.getColumnName(6);
			    	//rsmd.getColumnName(7);
			    	//rsmd.getColumnName(8);
			    	//rsmd.getColumnName(9);
			    	//rsmd.getColumnName(10);
			    	//rsmd.getColumnName(11);
			    	//rsmd.getColumnName(12);
			    	
			    	int i=0;
			    	while(rs.next())
			    	{
			    		//s[i][0]=rs.getString(1);
						s[i][0]=rs.getString(2);
						s[i][1]=rs.getString(3);
						s[i][2]=rs.getString(4);
						s[i][3]=rs.getString(5);
						s[i][4]=rs.getString(6);
						s[i][5]=rs.getString(7);
						//s[i][6]=rs.getString(8);
						//s[i][7]=rs.getString(9);
						//s[i][8]=rs.getString(10);
						//s[i][9]=rs.getString(11);
						i++;
									
			    		
//.setText(rs.getString("Id"));
			    	}
			    	
				}
				catch (Exception a)
				{
					
					
				}
				
				
				
				
				String[] columns=new String[06];
				//columns[0]="userid";
				//columns[0]="BookCode";
				//columns[0]="FName";
				//columns[0]="Class";
				//columns[0]="Shift";
				//columns[0]="Progress";
				//columns[0]="Address";
				//columns[0]="PhoneNumber";
				//columns[0]="UserID";
				columns[0]="BookID";
				columns[1]="Issued Date";
				columns[2]="BookIssue 1";
				columns[3]="BookIssue 2";
				columns[4]="Return Date";
				columns[5]="Book Returned";
				//columns[6]="Address";
				//columns[7]="Phone Number";
				//columns[8]="Email Address";
				//columns[9]="City";
				//columns[11]="rttaioisd";
				
				//table.getColumnModel().getColumn(1);
				table.setModel(new DefaultTableModel(s,columns));
	//table.getModel().getColumnName(1);
			}
		});
		btnCheckStudentRecord.setBounds(35, 143, 299, 23);
		contentPane.add(btnCheckStudentRecord);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 214, 573, 56);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JLabel label = new JLabel("Note : \"0\" Means The Book Is Not Returned , And \"1\" Means That You Have Returned The Books !");
		label.setForeground(Color.RED);
		label.setBounds(10, 277, 574, 14);
		contentPane.add(label);
		
		JButton btnBackToOptions = new JButton("Back To Options !");
		btnBackToOptions.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				adm_choose ac=new adm_choose();
				ac.setVisible(true);
			}
		});
		btnBackToOptions.setBounds(0, 0, 142, 23);
		contentPane.add(btnBackToOptions);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon("C:\\Documents and Settings\\Administrator\\Desktop\\Pics\\1731-books-library-wallpaper.jpg"));
		lblNewLabel_1.setBounds(0, 0, 573, 294);
		contentPane.add(lblNewLabel_1);
	}
}
