import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import java.awt.Toolkit;
import java.awt.Color;


public class choose extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					choose frame = new choose();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public choose() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Documents and Settings\\Administrator\\Desktop\\bk pics\\java prjct\\icon.gif"));
		setTitle("Library Management System");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 405, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Administrator Panel");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			adm_login a1=new adm_login();
		a1.setVisible(true);
		setVisible(false);
			
			}
		});
		btnNewButton.setBounds(28, 100, 176, 35);
		contentPane.add(btnNewButton);
		
		JButton btnUserPanel = new JButton("Student Panel");
		btnUserPanel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				StdLogin s1=new StdLogin();
				s1.setVisible(true);
				setVisible(false);
			}
		});
		btnUserPanel.setBounds(219, 100, 151, 35);
		contentPane.add(btnUserPanel);
		
		JLabel lblNoteAdministratorPanel = new JLabel("Note: Administrator Panel Will Only Be Used By Concerned Officials !");
		lblNoteAdministratorPanel.setForeground(Color.WHITE);
		lblNoteAdministratorPanel.setFont(new Font("Tahoma", Font.PLAIN, 9));
		lblNoteAdministratorPanel.setBounds(10, 252, 343, 14);
		contentPane.add(lblNoteAdministratorPanel);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setVerticalAlignment(SwingConstants.BOTTOM);
		lblNewLabel.setIcon(new ImageIcon("C:\\Documents and Settings\\Administrator\\Desktop\\bk pics\\bk (5).jpg"));
		lblNewLabel.setBounds(0, 0, 400, 266);
		contentPane.add(lblNewLabel);
	}
}
