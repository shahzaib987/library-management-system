import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.*;
import javax.swing.ImageIcon;

public class adm_login extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					adm_login frame = new adm_login();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public adm_login() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Documents and Settings\\Administrator\\Desktop\\bk pics\\java prjct\\icon.gif"));
		setResizable(false);
		setTitle("Administrator Login Page");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAdminId = new JLabel("Admin ID:");
		lblAdminId.setBounds(167, 100, 83, 14);
		contentPane.add(lblAdminId);
		
		JLabel lblAdminPassword = new JLabel("Admin Password:");
		lblAdminPassword.setBounds(117, 135, 108, 14);
		contentPane.add(lblAdminPassword);
		
		textField = new JTextField();
		textField.setBounds(219, 97, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(219, 132, 86, 20);
		contentPane.add(passwordField);
		
		JLabel lblForOfficialUse = new JLabel("For Official Use Only !");
		lblForOfficialUse.setFont(new Font("Tahoma", Font.PLAIN, 9));
		lblForOfficialUse.setBounds(337, 0, 116, 14);
		contentPane.add(lblForOfficialUse);
		
		JButton btnNewButton = new JButton("Login !");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Connection con=null;
				try {
				      Class.forName("sun.jdbc.odbc.JdbcOdbcDriver") ;

				// Connect with a full url string
				         con = DriverManager.getConnection("jdbc:odbc:Student");
				      //JOptionPane.showMessageDialog(null, "Database Connection Confirmed!");
				      	Statement stmt=con.createStatement();
				    	con.createStatement();
				    	ResultSet rs=stmt.executeQuery("select count(*) from admintbl where AdminUser = '" +textField.getText() + "' and AdminPass='" + String.copyValueOf(passwordField.getPassword()) + "'");
				    	
				    	//ResultSet rs=stmt.executeQuery("SELECT BookIssue.IssuedDate, BookIssue.ReturnDate FROM StdLogin INNER JOIN BookIssue ON StdLogin.userID = BookIssue.userID WHERE BookIssue.ReturnDate>GETDATE() AND Username='" +textField.getText()+ "'");
				    	//ResultSet alr=stmt.executeQuery("select ReturnDate from user_inf where Username='" +StdLogin.textField.getText()+ "'");
				    	//ResultSet ss=ssss.executeQuery("select count(*) from BookIssue where ReturnDate>DATEPART(DAY,GETDATE()) and Username='" +textField.getText()+ "'"); 
				    				    	
				    	rs.next();
				    	
				    	//ss.next();

				    	/*if(ss.getInt(1)==1)
				    	{
				    		
				    		JOptionPane.showMessageDialog(null, "please return a book!");
				    	}
				    	*/
				    	
				    	
				 if(rs.getInt(1)==1)
				    	{
				    		JOptionPane.showMessageDialog(null, "Thank you sir, you may now proceed to Administrator Panel !");
				    		
				    		adm_choose ap=new adm_choose();
				    		ap.setVisible(true);
				    		setVisible(false);
				    		
				    		}
				    		
				    	
				    	else
				    	{Toolkit.getDefaultToolkit().beep();
				    		JOptionPane.showMessageDialog(null, "Administrator Login Error , Please Check Username/Password !");
				    	}
				    	
				    	
				    	/*Date dateAndTime1 = Calendar.getInstance().getTime();
				    	if(dateAndTime1.getDate()>=alr.getInt("ReturnDate"))
				    	{
				    		
				    		JOptionPane.showMessageDialog(null, "Please Return The Books !");
				    		return_book b1= new return_book();
				    		b1.setVisible(true);
				    		setVisible(false);
				  
				    		
				    	}	    	
				    	*/
				
				      con.close();
				
				     // if()
				    } catch (Exception e1) {
				    	
				    }
				
				  }
				
			
		});
		btnNewButton.setBounds(117, 163, 89, 23);
		contentPane.add(btnNewButton);
		
		JButton btnExit = new JButton("Exit !");
		btnExit.setIcon(new ImageIcon("C:\\Documents and Settings\\Administrator\\Desktop\\College Library Record Application\\EXIT.PNG"));
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			dispose();
			}
		});
		btnExit.setBounds(219, 163, 94, 23);
		contentPane.add(btnExit);
		
		JButton btnBackToPanel = new JButton("Back To Panel Chooser !");
		btnBackToPanel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				choose ac=new choose();
				ac.setVisible(true);
				setVisible(false);
			}
		});
		btnBackToPanel.setBounds(0, 0, 179, 23);
		contentPane.add(btnBackToPanel);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\Documents and Settings\\Administrator\\Desktop\\Pics\\1731-books-library-wallpaper.jpg"));
		lblNewLabel.setBounds(0, 0, 444, 268);
		contentPane.add(lblNewLabel);
	}
}
