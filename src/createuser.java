import java.awt.*;
//import javax.sql.RowSetReader;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
//import javax.swing.text.TableView.TableRow;
import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

//import javax.swing.table.*;
//import java.awt.*;

public class createuser extends JFrame {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPasswordField passwordField;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					createuser frame = new createuser();
					frame.setVisible(true);
					frame.setPreferredSize(new Dimension(450,390));
					frame.pack();
					frame.setLocationRelativeTo(null);
					
					
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public createuser() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Documents and Settings\\Administrator\\Desktop\\bk pics\\java prjct\\icon.gif"));
		setResizable(false);
		setTitle("Student Create ID Form");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 463, 391);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Note: Enter Student ID and Password Carefully !");
		label.setForeground(Color.DARK_GRAY);
		label.setFont(new Font("Tahoma", Font.PLAIN, 9));
		label.setBounds(50, 345, 347, 14);
		contentPane.add(label);
		
		JButton button = new JButton("Create Student ID");
		button.setBounds(66, 228, 157, 23);
		contentPane.add(button);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(216, 150, 181, 20);
		contentPane.add(passwordField);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(216, 108, 181, 20);
		contentPane.add(textField);
		
		JLabel label_1 = new JLabel("Password:");
		label_1.setBounds(82, 153, 63, 14);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("Student ID:");
		label_2.setBounds(84, 110, 122, 17);
		contentPane.add(label_2);
		
		JButton btnExit = new JButton("Exit !");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			dispose();
			}
		});
		btnExit.setBounds(249, 228, 157, 23);
		contentPane.add(btnExit);
		
		JButton btnBackToStudent = new JButton("Back to Student Login Page !");
		btnBackToStudent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				user_panel up=new user_panel();
				up.setVisible(true);
				setVisible(false);
			}
		});
		btnBackToStudent.setBounds(0, 0, 171, 23);
		contentPane.add(btnBackToStudent);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\Documents and Settings\\Administrator\\Desktop\\bk pics\\bk (1).jpg"));
		lblNewLabel.setBounds(0, 0, 457, 359);
		contentPane.add(lblNewLabel);
	}
}
