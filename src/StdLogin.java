import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.*;
import java.text.SimpleDateFormat;

import javax.swing.JPasswordField;
import javax.swing.JFormattedTextField;
import java.awt.Font;
import javax.swing.ImageIcon;
import java.awt.Color;


public class StdLogin extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	public static JTextField textField;
	private JPasswordField passwordField;
	public static JFormattedTextField formattedTextField;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StdLogin frame = new StdLogin();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
		@SuppressWarnings("deprecation")
		public StdLogin() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Documents and Settings\\Administrator\\Desktop\\bk pics\\java prjct\\icon.gif"));
		setTitle("LMS Student Login Page");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 267);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Student ID:");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setBounds(29, 56, 89, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Library Card Number:");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setBounds(29, 79, 132, 14);
		contentPane.add(lblNewLabel_1);
		
		JButton btnNewButton = new JButton("Login In");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Connection con=null;
				try {
				      Class.forName("sun.jdbc.odbc.JdbcOdbcDriver") ;

				// Connect with a full url string
				         con = DriverManager.getConnection("jdbc:odbc:Student");
				      //JOptionPane.showMessageDialog(null, "Database Connection Confirmed!");
				      	Statement stmt=con.createStatement();
				    	con.createStatement();
				    	ResultSet rs=stmt.executeQuery("select count(*) from StdLogin where Username = '" +textField.getText() + "' and Password='" + String.copyValueOf(passwordField.getPassword()) + "'");
				    	
				    	//ResultSet rs=stmt.executeQuery("SELECT BookIssue.IssuedDate, BookIssue.ReturnDate FROM StdLogin INNER JOIN BookIssue ON StdLogin.userID = BookIssue.userID WHERE BookIssue.ReturnDate>GETDATE() AND Username='" +textField.getText()+ "'");
				    	//ResultSet alr=stmt.executeQuery("select ReturnDate from user_inf where Username='" +StdLogin.textField.getText()+ "'");
				    	//ResultSet ss=ssss.executeQuery("select count(*) from BookIssue where ReturnDate>DATEPART(DAY,GETDATE()) and Username='" +textField.getText()+ "'"); 
				    	
				    	
				    	
				    	rs.next();
				    	
				    	//ss.next();

				    	/*if(ss.getInt(1)==1)
				    	{
				    		
				    		JOptionPane.showMessageDialog(null, "please return a book!");
				    	}
				    	*/
				    	
				    	
				 if(rs.getInt(1)==1)
				    	{
				    		JOptionPane.showMessageDialog(null, "User Correct, you can now proceed to your panel !");
				    		
				    		user_panel p=new user_panel();
				    		p.setVisible(true);
				    		setVisible(false);
				    		
				    		}
				    		
				    	
				    	else
				    	{Toolkit.getDefaultToolkit().beep();
				    		JOptionPane.showMessageDialog(null, "User In-Correct , please insert the correct Username/Password !");
				    	}
				    	
				    	
				    	/*Date dateAndTime1 = Calendar.getInstance().getTime();
				    	if(dateAndTime1.getDate()>=alr.getInt("ReturnDate"))
				    	{
				    		
				    		JOptionPane.showMessageDialog(null, "Please Return The Books !");
				    		return_book b1= new return_book();
				    		b1.setVisible(true);
				    		setVisible(false);
				  
				    		
				    	}	    	
				    	*/
				
				      con.close();
				
				     // if()
				    } catch (Exception e) {
				    	
				    }
				
				  }
				
			
		});
		btnNewButton.setBounds(83, 135, 132, 30);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Create Student ID");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				createuser h2=new createuser();
				h2.setVisible(true);
				setVisible(false);
			}
		});
		
		
		
		btnNewButton_1.setBounds(225, 135, 149, 30);
		contentPane.add(btnNewButton_1);
		
		textField = new JTextField();
		textField.setBounds(171, 46, 184, 25);
		contentPane.add(textField);
		textField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(171, 78, 184, 25);
		contentPane.add(passwordField);
		
		JFormattedTextField formattedTextField_1 = new JFormattedTextField();
		formattedTextField_1= new JFormattedTextField(new SimpleDateFormat("dd/MM/yyyy"));
		formattedTextField_1.setEnabled(false);
		formattedTextField_1.setEditable(false);
		formattedTextField_1.setValue(new java.util.Date());
		formattedTextField_1.setBounds(319, 11, 105, 20);
		contentPane.add(formattedTextField_1);
		
		JLabel lblNoteEnter = new JLabel("Note : Enter your Name and Password Correctly , Wrong Enteries May Cause An Error !");
		lblNoteEnter.setForeground(Color.WHITE);
		lblNoteEnter.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblNoteEnter.setBounds(10, 214, 434, 14);
		contentPane.add(lblNoteEnter);
		
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setIcon(new ImageIcon("C:\\Documents and Settings\\Administrator\\Desktop\\Pics\\948068202.jpg"));
		lblNewLabel_2.setBounds(0, 0, 442, 233);
		contentPane.add(lblNewLabel_2);
		formattedTextField_1.hide();
	}
}
