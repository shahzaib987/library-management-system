import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.*;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import java.awt.Color;

public class insertbooks extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField bname;
	private JTextField aname;
	private JTextField pb;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					insertbooks frame = new insertbooks();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
					System.out.println("This is test");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public insertbooks() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Documents and Settings\\Administrator\\Desktop\\bk pics\\java prjct\\icon.gif"));
		setResizable(false);
		setTitle("Library Management System ( Insert Book Panel ) ");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 458, 336);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Book Name :");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setBounds(98, 44, 94, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Author Name :");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setBounds(97, 99, 108, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Price Of Book :");
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setBounds(98, 163, 94, 14);
		contentPane.add(lblNewLabel_2);
		
		bname = new JTextField();
		bname.setBounds(202, 41, 125, 20);
		contentPane.add(bname);
		bname.setColumns(10);
		
		aname = new JTextField();
		aname.setBounds(202, 96, 125, 20);
		contentPane.add(aname);
		aname.setColumns(10);
		
		pb = new JTextField();
		
		pb.setBounds(202, 160, 125, 20);
		contentPane.add(pb);
		pb.setColumns(10);
		
		JButton btnNewButton = new JButton("Insert The Books !");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
					Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
					Connection cn=DriverManager.getConnection("jdbc:odbc:Student");
					Statement stmt=cn.createStatement();
					JOptionPane.showMessageDialog(null,"Insert Books Successfully !");
					ResultSet rs=stmt.executeQuery("insert into BookDetail(BookName, AuthorName, Price) values('" +bname.getText() + "','" + aname.getText()+ "','" + pb.getText()+"')");
					
					rs.next();
				   

					cn.close();
					stmt.close();
			 	}
				catch (Exception a)
				{
					
					
				}
			}
		});
		btnNewButton.setBounds(55, 226, 168, 33);
		contentPane.add(btnNewButton);
		
		JButton exit = new JButton("Exit !");
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			dispose();
			}
		});
		exit.setBounds(243, 226, 161, 33);
		contentPane.add(exit);
		
		JButton btnNewButton_1 = new JButton("Back To Options !");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			adm_choose ac=new adm_choose();
			ac.setVisible(true);
			setVisible(false);
			}
		});
		btnNewButton_1.setBounds(0, 0, 136, 20);
		contentPane.add(btnNewButton_1);
		
		JLabel label = new JLabel("New label");
		label.setIcon(new ImageIcon("C:\\Documents and Settings\\Administrator\\Desktop\\Pics\\1731-books-library-wallpaper.jpg"));
		label.setBounds(0, 0, 452, 304);
		contentPane.add(label);
	}
}
