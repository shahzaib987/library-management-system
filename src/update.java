import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.*;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import java.awt.SystemColor;

public class update extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField name;
	private JTextField phone;
	private JTextField city;
	private JTextField progress;
	private JTextField class1;
	private JTextField eadd;
	private JTextField padd;
	private JTextField username;
	private JButton detail;
	private JScrollPane scrollPane;
	private JTable table;
	private JButton btnExit;
	private JButton btnNewButton;
	private JButton btnBackToOptions;
	private JTextField shift;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					update frame = new update();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public update() {
		setBackground(SystemColor.activeCaptionBorder);
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Documents and Settings\\Administrator\\Desktop\\bk pics\\java prjct\\icon.gif"));
		setTitle("Library Management System (Update Panel)");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 824, 581);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLUE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		shift = new JTextField();
		shift.setForeground(Color.BLACK);
		shift.setBackground(Color.WHITE);
		shift.setBounds(174, 259, 152, 26);
		contentPane.add(shift);
		
		name = new JTextField();
		name.setForeground(Color.BLACK);
		name.setBackground(Color.WHITE);
		name.setBounds(174, 163, 152, 26);
		contentPane.add(name);
		
		phone = new JTextField();
		phone.setForeground(Color.BLACK);
		phone.setBackground(Color.WHITE);
		phone.setBounds(525, 163, 152, 26);
		contentPane.add(phone);
		
		city = new JTextField();
		city.setForeground(Color.BLACK);
		city.setBackground(Color.WHITE);
		city.setBounds(525, 211, 152, 26);
		contentPane.add(city);
		
		JLabel label = new JLabel("City");
		label.setForeground(Color.WHITE);
		label.setBounds(365, 214, 150, 20);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("Enter Phone Number :");
		label_1.setForeground(Color.WHITE);
		label_1.setBounds(365, 166, 150, 20);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("Student's Name :");
		label_2.setForeground(Color.WHITE);
		label_2.setBounds(76, 166, 150, 20);
		contentPane.add(label_2);
		
		JLabel label_3 = new JLabel("Class");
		label_3.setForeground(Color.WHITE);
		label_3.setBounds(76, 214, 150, 20);
		contentPane.add(label_3);
		
		progress = new JTextField();
		progress.setForeground(Color.BLACK);
		progress.setBackground(Color.WHITE);
		progress.setBounds(174, 310, 152, 26);
		contentPane.add(progress);
		
		JLabel label_4 = new JLabel("Shift");
		label_4.setForeground(Color.WHITE);
		label_4.setBounds(76, 262, 150, 20);
		contentPane.add(label_4);
		
		JLabel label_5 = new JLabel("Progress");
		label_5.setForeground(Color.WHITE);
		label_5.setBounds(76, 313, 150, 20);
		contentPane.add(label_5);
		
		class1 = new JTextField();
		class1.setForeground(Color.BLACK);
		class1.setBackground(Color.WHITE);
		class1.setBounds(174, 211, 152, 26);
		contentPane.add(class1);
		
		JLabel label_6 = new JLabel("Email Address");
		label_6.setForeground(Color.WHITE);
		label_6.setBounds(365, 313, 150, 20);
		contentPane.add(label_6);
		
		eadd = new JTextField();
		eadd.setForeground(Color.BLACK);
		eadd.setBackground(Color.WHITE);
		eadd.setBounds(525, 310, 152, 26);
		contentPane.add(eadd);
		
		padd = new JTextField();
		padd.setForeground(Color.BLACK);
		padd.setBackground(Color.WHITE);
		padd.setBounds(525, 259, 152, 26);
		contentPane.add(padd);
		
		JLabel label_7 = new JLabel("Permenent Address");
		label_7.setForeground(Color.WHITE);
		label_7.setBounds(365, 262, 150, 20);
		contentPane.add(label_7);
		
		JButton btnUpdate = new JButton("Update a Record !");
		btnUpdate.setIcon(new ImageIcon("C:\\Documents and Settings\\Administrator\\Desktop\\College Library Record Application\\update.gif"));
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try
				{
					Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
					Connection cn=DriverManager.getConnection("jdbc:odbc:Student");
					cn.createStatement();
					JOptionPane.showMessageDialog(null,"Successfully Updated The Student Record !");
					//ResultSet rs=stmt.executeQuery("Select count(*) from login_table where Userid='"+textField.getText()+"'and Password='"+String.copyValueOf(passwordField.getPassword())+"'");
					PreparedStatement ps=cn.prepareStatement ("Update StdLogin set FName=?, Class=?, Shift=?, Progress=?, City=?, Address=?, PhoneNumber=?, EmailAddress=? where Username='" +username.getText()+ "'");
					
					ps.setString(1,name.getText());		
					ps.setString(2,class1.getText());
					ps.setString(3,shift.getText());
					ps.setString(4,progress.getText());
					ps.setString(5,city.getText());
					ps.setString(6,padd.getText());
					ps.setString(7,phone.getText());
					ps.setString(8,eadd.getText());
					ps.executeUpdate();
					
					
					/*if(rs.getInt(1)==1)
					{
					JOptionPane.showMessageDialog(null,"Username Or Password Is Correct");
					z2 w=new z2();
					w.setVisible(true);
					setVisible(false);
					}
					
					else
					{
						JOptionPane.showMessageDialog(null,"Username Or Password Is Not Correct");
						
					}*/cn.commit();
					cn.close();
					}	
				
				catch(Exception e1)
				{
					
				}
				}
					
				
		});
		 
		btnUpdate.setBounds(237, 374, 203, 31);
		contentPane.add(btnUpdate);
		
		JLabel lblSdfsdf = new JLabel("Enter Your Username:");
		lblSdfsdf.setForeground(Color.WHITE);
		lblSdfsdf.setBounds(10, 50, 108, 14);
		contentPane.add(lblSdfsdf);
		
		username = new JTextField();
		username.setBounds(140, 47, 86, 20);
		contentPane.add(username);
		username.setColumns(10);
		
		detail = new JButton("Search Student's Detail !");
		detail.setIcon(new ImageIcon("C:\\Documents and Settings\\Administrator\\Desktop\\College Library Record Application\\search.gif"));
		detail.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
			    {
			       Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			       Connection c = DriverManager.getConnection("jdbc:odbc:Student");
			       Statement st = c.createStatement();
			       JOptionPane.showMessageDialog(null,"Searching For Student Profile Information !");
			       ResultSet rs = st.executeQuery("select * from StdLogin where Username='" +username.getText()+ "'");
			      
			       
			       
			       
			       
			       while(rs.next())
			       {	
			    	  
						  	   // date1.setText(rs.getString("DateToday"));	
				    	    //id.setText(rs.getString("Id"));
						name.setText(rs.getString("FName"));
				    	padd.setText(rs.getString("Address"));
					    class1.setText(rs.getString("Class"));
					    progress.setText(rs.getString("Progress"));
					    phone.setText(rs.getString("PhoneNumber"));
					    shift.setText(rs.getString("Shift"));
					    city.setText(rs.getString("City"));
					   // padd.setText(rs.getString("Permenent_Address"));
					    eadd.setText(rs.getString("EmailAddress"));
					    
					    
			      }
			      
			       
			       c.close();
			        	st.close();
			    }
				
			        	catch(Exception e1)
						{
			        		
						}
							
				}
			});
		detail.setBounds(237, 38, 203, 31);
		contentPane.add(detail);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 452, 816, 95);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		btnExit = new JButton("Exit !");
		btnExit.setIcon(new ImageIcon("C:\\Documents and Settings\\Administrator\\Desktop\\College Library Record Application\\remove.gif"));
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			dispose();
			}
		});
		btnExit.setBounds(503, 374, 108, 31);
		contentPane.add(btnExit);
		
		btnNewButton = new JButton("Show The Student Record In Database Field Below  !");
		btnNewButton.setIcon(new ImageIcon("C:\\Documents and Settings\\Administrator\\Desktop\\College Library Record Application\\SEARCH.PNG"));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Object[][] s=new Object[1][11];
				try
			    {
			       Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			       Connection c = DriverManager.getConnection("jdbc:odbc:Student");
			       Statement st = c.createStatement();
			       JOptionPane.showMessageDialog(null,"Showing The Student Information In Database Below !");
			       ResultSet rs = st.executeQuery("select * from StdLogin where Username='" +username.getText()+ "'");
			      		       
			       int i=0;
			       
			       
			       while(rs.next())
			       {	
			    	  
			    	   s[i][0]=rs.getString(1);
						s[i][1]=rs.getString(2);
						s[i][2]=rs.getString(3);
						s[i][3]=rs.getString(4);
						s[i][4]=rs.getString(5);
						s[i][5]=rs.getString(6);
						s[i][6]=rs.getString(7);
						s[i][7]=rs.getString(8);
						s[i][8]=rs.getString(9);
						s[i][9]=rs.getString(10);
						s[i][10]=rs.getString(11);
						i++; 
					    					    
					    
			      }
			      
			       
			       c.close();
			        	st.close();
			    }
				
			        	catch(Exception e1)
						{
			        		
						}
				String[] columns=new String[11];
				//columns[0]="userid";
				//columns[0]="BookCode";
				//columns[0]="FName";
				//columns[0]="Class";
				//columns[0]="Shift";
				//columns[0]="Progress";
				//columns[0]="Address";
				//columns[0]="PhoneNumber";
				columns[0]="UserID";
				columns[1]="Username";
				columns[2]="Password";
				columns[3]="Full Name";
				columns[4]="Progress";
				columns[5]="Shift";
				columns[6]="Progress";
				columns[7]="Address";
				columns[8]="Phone Number";
				columns[9]="Email Address";
				columns[10]="City";
				//columns[11]="rttaioisd";
		
				table.setModel(new DefaultTableModel(s,columns));
				}
			});
			
		
		btnNewButton.setBounds(450, 38, 356, 31);
		contentPane.add(btnNewButton);
		
		btnBackToOptions = new JButton("Back To Options !");
		btnBackToOptions.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				adm_choose ac=new adm_choose();
				ac.setVisible(true);
				setVisible(false);
			}
		});
		btnBackToOptions.setBounds(0, 0, 152, 23);
		contentPane.add(btnBackToOptions);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\Documents and Settings\\administrator.APTECH\\Desktop\\Pics\\1731-books-library-wallpaper.jpg"));
		lblNewLabel.setBounds(0, 0, 816, 547);
		contentPane.add(lblNewLabel);
	}
}
