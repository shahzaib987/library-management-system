import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JFormattedTextField;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.Toolkit;

public class user_panel extends JFrame {

	/**
	 * 
	 */
	public static JLabel alert_1;
	public static JLabel label;
	public static JLabel rtl_1;
	public static JButton rbutton_1;
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	public static JTextField textField;
	public static JFormattedTextField formattedTextField;
	private JTable table;
	public static JTextField rd;
	public static JTextField sd;
	private static JTextField br;
	

	/**
	 * Launch the application.
	 */
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			
			public void run() {
				try	
				{
				user_panel frame = new user_panel();
				frame.setVisible(true);
				frame.setLocationRelativeTo(null);
				
				
				}
				catch (Exception e) {
					e.printStackTrace();
					       		
				}
				
		}
	});
}
	
	/**
	 * Create the frame.
	 */
	@SuppressWarnings("deprecation")
	public user_panel() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Documents and Settings\\Administrator\\Desktop\\bk pics\\java prjct\\icon.gif"));
		
		  
		setTitle("Library Management System ( Student Panel )");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 661, 556);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		final JButton rbutton_1 = new JButton("Click To Return Book !");
		rbutton_1.setFont(new Font("Tahoma", Font.BOLD, 9));
		rbutton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				return_book rb=new return_book();
		    	 rb.setVisible(true);
		    	 setVisible(false);
			}
		});
		rbutton_1.hide();
		
		final JLabel alert_1_1 = new JLabel("");
		alert_1_1.setEnabled(true);
		alert_1_1.setBackground(new Color(255, 255, 255));
		alert_1_1.setIcon(new ImageIcon("C:\\Documents and Settings\\Administrator\\Desktop\\alert_animated.gif"));
		alert_1_1.setBounds(272, 11, 107, 69);
		contentPane.add(alert_1_1);
		alert_1_1.hide();
		
		JLabel lblToYourPanel = new JLabel("to your panel !");
		lblToYourPanel.setForeground(Color.BLACK);
		lblToYourPanel.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		lblToYourPanel.setBounds(161, 40, 150, 40);
		contentPane.add(lblToYourPanel);
		
		JLabel lblNoteWhen = new JLabel("Note : When The Column \"BookReturned' Is 1, It means you have returned the book(s) !");
		lblNoteWhen.setForeground(Color.RED);
		lblNoteWhen.setBounds(70, 502, 506, 14);
		contentPane.add(lblNoteWhen);
		rbutton_1.setBounds(424, 103, 212, 23);
		contentPane.add(rbutton_1);
		
		final JLabel rtl_1 = new JLabel("Please Return The Book(s) !");
		rtl_1.setForeground(Color.RED);
		rtl_1.setFont(new Font("Century Gothic", Font.BOLD, 16));
		rtl_1.setBounds(376, 33, 263, 37);
		contentPane.add(rtl_1);
		rtl_1.hide();
		
		JLabel lblWelcome = new JLabel("Welcome");
		lblWelcome.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		lblWelcome.setForeground(new Color(0, 0, 0));
		lblWelcome.setBounds(21, 40, 73, 40);
		contentPane.add(lblWelcome);
		
		JLabel label = new JLabel("1");
		label=new JLabel(StdLogin.textField.getText());	
		label.setForeground(Color.BLACK);
		label.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		label.setBounds(93, 40, 109, 40);
		contentPane.add(label);
		
		JButton btnCheckMyBook = new JButton("Check My Book Issued !");
		btnCheckMyBook.setFont(new Font("Tahoma", Font.BOLD, 9));
		btnCheckMyBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Object[][] s=new Object[1][06];
				try
				{
				    Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			    	Connection cn=DriverManager.getConnection("jdbc:odbc:Student");
			    	Statement stmt=cn.createStatement();
			    	 JOptionPane.showMessageDialog(null," Your Book Issued !");
			    	ResultSet rs=stmt.executeQuery("select *  from  BookIssue where Username1='" +StdLogin.textField.getText()+ "'");
			    	//ResultSetMetaData rsmd = rs.getMetaData();
			    	//rsmd.getColumnName(1);
			    	//rsmd.getColumnName(2);
			    	//rsmd.getColumnName(3);
			    	//rsmd.getColumnName(4);
			    	//rsmd.getColumnName(5);
			    	//rsmd.getColumnName(6);
			    	//rsmd.getColumnName(7);
			    	//rsmd.getColumnName(8);
			    	//rsmd.getColumnName(9);
			    	//rsmd.getColumnName(10);
			    	//rsmd.getColumnName(11);
			    	//rsmd.getColumnName(12);
			    	
			    	int i=0;
			    	while(rs.next())
			    	{
			    		//s[i][0]=rs.getString(1);
						s[i][0]=rs.getString(2);
						s[i][1]=rs.getString(3);
						s[i][2]=rs.getString(4);
						s[i][3]=rs.getString(5);
						s[i][4]=rs.getString(6);
						s[i][5]=rs.getString(7);
						//s[i][6]=rs.getString(8);
						//s[i][7]=rs.getString(9);
						//s[i][8]=rs.getString(10);
						//s[i][9]=rs.getString(11);
						i++;
									
			    		
//.setText(rs.getString("Id"));
			    	}
			    	
				}
				catch (Exception a)
				{
					
					
				}
				
				
				
				
				String[] columns=new String[06];
				//columns[0]="userid";
				//columns[0]="BookCode";
				//columns[0]="FName";
				//columns[0]="Class";
				//columns[0]="Shift";
				//columns[0]="Progress";
				//columns[0]="Address";
				//columns[0]="PhoneNumber";
				//columns[0]="UserID";
				columns[0]="BookID";
				columns[1]="Issued Date";
				columns[2]="BookIssue 1";
				columns[3]="BookIssue 2";
				columns[4]="Return Date";
				columns[5]="Book Returned";
				//columns[6]="Address";
				//columns[7]="Phone Number";
				//columns[8]="Email Address";
				//columns[9]="City";
				//columns[11]="rttaioisd";
				
				//table.getColumnModel().getColumn(1);
				table.setModel(new DefaultTableModel(s,columns));
	//table.getModel().getColumnName(1);
			}
		});
		btnCheckMyBook.setBounds(21, 189, 226, 40);
		contentPane.add(btnCheckMyBook);
		
		JButton btnCheckMyInformation = new JButton("Check My Registration Information !");
		btnCheckMyInformation.setFont(new Font("Tahoma", Font.BOLD, 9));
		btnCheckMyInformation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Object[][] s=new Object[1][11];
				try
				{
				    Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			    	Connection cn=DriverManager.getConnection("jdbc:odbc:Student");
			    	Statement stmt=cn.createStatement();
			    	 JOptionPane.showMessageDialog(null,"Your Registration Information !");
			    	ResultSet rs=stmt.executeQuery("select *  from  StdLogin where Username='" +StdLogin.textField.getText()+ "'");
			    	//ResultSetMetaData rsmd = rs.getMetaData();
			    	//rsmd.getColumnName(1);
			    	//rsmd.getColumnName(2);
			    	//rsmd.getColumnName(3);
			    	//rsmd.getColumnName(4);
			    	//rsmd.getColumnName(5);
			    	//rsmd.getColumnName(6);
			    	//rsmd.getColumnName(7);
			    	//rsmd.getColumnName(8);
			    	//rsmd.getColumnName(9);
			    	//rsmd.getColumnName(10);
			    	//rsmd.getColumnName(11);
			    	//rsmd.getColumnName(12);
			    	
			    	int i=0;
			    	while(rs.next())
			    	{
			    		s[i][0]=rs.getString(1);
						s[i][1]=rs.getString(2);
						s[i][2]=rs.getString(3);
						s[i][3]=rs.getString(4);
						s[i][4]=rs.getString(5);
						s[i][5]=rs.getString(6);
						s[i][6]=rs.getString(7);
						s[i][7]=rs.getString(8);
						s[i][8]=rs.getString(9);
						s[i][9]=rs.getString(10);
						s[i][10]=rs.getString(11);
						i++;
			    		
//.setText(rs.getString("Id"));
			    	}
			    	
				}
				catch (Exception a)
				{
					
					
				}
				
				
				
				
				String[] columns=new String[11];
				//columns[0]="userid";
				//columns[0]="BookCode";
				//columns[0]="FName";
				//columns[0]="Class";
				//columns[0]="Shift";
				//columns[0]="Progress";
				//columns[0]="Address";
				//columns[0]="PhoneNumber";
				columns[0]="UserID";
				columns[1]="Username";
				columns[2]="Password";
				columns[3]="Full Name";
				columns[4]="Progress";
				columns[5]="Shift";
				columns[6]="Progress";
				columns[7]="Address";
				columns[8]="Phone Number";
				columns[9]="Email Address";
				columns[10]="City";
				//columns[11]="rttaioisd";
				
				//table.getColumnModel().getColumn(1);
				table.setModel(new DefaultTableModel(s,columns));
	//table.getModel().getColumnName(1);
			}
		});
			
		btnCheckMyInformation.setBounds(21, 122, 226, 40);
		contentPane.add(btnCheckMyInformation);
		
		JButton btnCheckMyStatus = new JButton("Check My Status Of Return Book !");
		btnCheckMyStatus.setFont(new Font("Tahoma", Font.BOLD, 9));
		btnCheckMyStatus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try
				{	
				    Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			    	Connection c=DriverManager.getConnection("jdbc:odbc:Student");
			    	Statement st=c.createStatement();
			    	 JOptionPane.showMessageDialog(null,"Checking Your Return Book Status........ ");
			    	 ResultSet rs = st.executeQuery("select * from BookIssue where Username1='" +StdLogin.textField.getText()+ "'");
			    	 
				       while(rs.next())
				       {		  // date1.setText(rs.getString("DateToday"));	
					    	    //id.setText(rs.getString("Id"));
						    //name.setText(rs.getString("FName"));
					    	//padd.setText(rs.getString("Address"));
						   // class1.setText(rs.getString("Class"));
						    //progress.setText(rs.getString("Progress"));
						    //phone.setText(rs.getString("PhoneNumber"));
						    //shift.setText(rs.getString("Shift"));
				    	   
				    	   rd.setText(rs.getString("ReturnDate"));
				    	   br.setText(rs.getString("BookReturned"));
				    	   //int value1 = Integer.parseInt(rd.getText());
						   //int value2 = Integer.parseInt(sd.getText());
				    	   
				    	   
				    	   
				    	   DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				    	   Date rtoday = df.parse(rd.getText());
				    	   
				    	   DateFormat sdate = new SimpleDateFormat("dd/MM/yyyy");
				    	   Date stoday = sdate.parse(sd.getText());
				    	
				    	   		    	  int brb= Integer.parseInt(br.getText()); 
				    	   		    	  
				    	   
				    	  // int results = rtoday.compareTo(stoday);
				    	   
				    	    if(stoday.after(rtoday) && brb==0)
				    	    {
				    	    	JOptionPane.showMessageDialog(null,"Kindly Return The Book(s) As Soon As Possible, The Return Date Of Your Book Exceeded !");
				    	    	JOptionPane.showMessageDialog(null,"Your Return Date Of Book Is : "+rd.getText());
				        		JOptionPane.showMessageDialog(null,"Today Is : "+sd.getText());
				        		JOptionPane.showMessageDialog(null,brb);
				        		alert_1_1.show();
				    	    	rtl_1.show();
				    	    	rtl_1.enable();
				    	    	rbutton_1.show();
				    	    	rbutton_1.enable();
				    	   
				    	    
				    	    }
				    	
				    	    else
				    	    {
				    	    	JOptionPane.showMessageDialog(null,"Your Have Already Return Your Books !");
				    	    	c.close();
						        	st.close();
				    	    }
				    	    
				    	   
				      }
				        	
				    }
				        	catch(Exception e1)
							{
				        		
				        		
							}
			}
			});
		btnCheckMyStatus.setBounds(21, 254, 226, 40);
		contentPane.add(btnCheckMyStatus);
		
		JFormattedTextField formattedTextField = new JFormattedTextField(new SimpleDateFormat("dd/MM/yyyy"));
		formattedTextField.setValue(new java.util.Date());
		//Date formattedTextField=formattedTextField.parse(s)
		
		formattedTextField.hide();
		
		formattedTextField.setEditable(false);
		formattedTextField.setEnabled(false);
		formattedTextField.setBounds(298, 424, 107, 20);
		contentPane.add(formattedTextField);
		
		rd = new JTextField();
		rd.setBounds(462, 424, 86, 20);
		contentPane.add(rd);
		rd.setColumns(10);
		rd.hide();
		sd = new JTextField();
		sd.setText(formattedTextField.getText());
		
		sd.setBounds(581, 424, 86, 20);
		contentPane.add(sd);
		sd.setColumns(10);
		sd.hide();
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 349, 658, 142);
		contentPane.add(scrollPane);
		
		
		table = new JTable();
		table.setBackground(Color.WHITE);
		scrollPane.setViewportView(table);
		
		br = new JTextField();
		br.setBounds(161, 424, 86, 20);
		contentPane.add(br);
		br.setColumns(10);
		
		JLabel lblForFurtherInformation = new JLabel("For Further Information Email To : syedshahzaibahmed@hotmail.com");
		lblForFurtherInformation.setForeground(Color.WHITE);
		lblForFurtherInformation.setFont(new Font("Tahoma", Font.PLAIN, 10));
		lblForFurtherInformation.setBounds(298, 0, 349, 14);
		contentPane.add(lblForFurtherInformation);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon("C:\\Documents and Settings\\Administrator\\Desktop\\bk pics\\bk (1).jpg"));
		lblNewLabel.setBounds(0, 0, 658, 524);
		contentPane.add(lblNewLabel);
		textField = new JTextField();
		br.hide();
		
	
	
	}
}


