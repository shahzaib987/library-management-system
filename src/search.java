import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.*;
import javax.swing.JScrollPane;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import java.awt.Color;


public class search extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField userName;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					search frame = new search();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public search() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Documents and Settings\\Administrator\\Desktop\\bk pics\\java prjct\\icon.gif"));
		setTitle("Library Management System (Search Panel)");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 776, 274);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel stdName = new JLabel("Enter Student Name:");
		stdName.setForeground(Color.WHITE);
		stdName.setBounds(237, 45, 134, 20);
		contentPane.add(stdName);
		
		userName = new JTextField();
		userName.setBounds(362, 41, 147, 28);
		contentPane.add(userName);
		userName.setColumns(10);
		
		JButton btnNewButton = new JButton("Show Me The Information Of Student !");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Object[][] s=new Object[1][11];
				try
				{
				    Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			    	Connection cn=DriverManager.getConnection("jdbc:odbc:Student");
			    	Statement stmt=cn.createStatement();
			    	 JOptionPane.showMessageDialog(null,"Showing Information Of Student  !");
			    	ResultSet rs=stmt.executeQuery("select *  from  StdLogin where Username='" +userName.getText()+ "'");
			    	//ResultSetMetaData rsmd = rs.getMetaData();
			    	//rsmd.getColumnName(1);
			    	//rsmd.getColumnName(2);
			    	//rsmd.getColumnName(3);
			    	//rsmd.getColumnName(4);
			    	//rsmd.getColumnName(5);
			    	//rsmd.getColumnName(6);
			    	//rsmd.getColumnName(7);
			    	//rsmd.getColumnName(8);
			    	//rsmd.getColumnName(9);
			    	//rsmd.getColumnName(10);
			    	//rsmd.getColumnName(11);
			    	//rsmd.getColumnName(12);
			    	
			    	int i=0;
			    	while(rs.next())
			    	{
			    		s[i][0]=rs.getString(1);
						s[i][1]=rs.getString(2);
						s[i][2]=rs.getString(3);
						s[i][3]=rs.getString(4);
						s[i][4]=rs.getString(5);
						s[i][5]=rs.getString(6);
						s[i][6]=rs.getString(7);
						s[i][7]=rs.getString(8);
						s[i][8]=rs.getString(9);
						s[i][9]=rs.getString(10);
						s[i][10]=rs.getString(11);
						i++;
			    		
//.setText(rs.getString("Id"));
			    	}
			    	
				}
				catch (Exception a)
				{
					
					
				}
				
				
				
				
				String[] columns=new String[11];
				//columns[0]="userid";
				//columns[0]="BookCode";
				//columns[0]="FName";
				//columns[0]="Class";
				//columns[0]="Shift";
				//columns[0]="Progress";
				//columns[0]="Address";
				//columns[0]="PhoneNumber";
				columns[0]="UserID";
				columns[1]="Username";
				columns[2]="Password";
				columns[3]="Full Name";
				columns[4]="Progress";
				columns[5]="Shift";
				columns[6]="Progress";
				columns[7]="Address";
				columns[8]="Phone Number";
				columns[9]="Email Address";
				columns[10]="City";
				//columns[11]="rttaioisd";
				
				//table.getColumnModel().getColumn(1);
				table.setModel(new DefaultTableModel(s,columns));
	//table.getModel().getColumnName(1);
			}
		});
		btnNewButton.setBounds(278, 108, 250, 23);
		contentPane.add(btnNewButton);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 178, 778, 62);
		contentPane.add(scrollPane);
		
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JButton btnNewButton_1 = new JButton("Back To Options !");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				adm_choose ac=new adm_choose();
				ac.setVisible(true);
				setVisible(false);
			}
		});
		btnNewButton_1.setBounds(0, 0, 134, 23);
		contentPane.add(btnNewButton_1);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon("C:\\Documents and Settings\\Administrator\\Desktop\\Pics\\1731-books-library-wallpaper.jpg"));
		label.setBounds(0, 0, 768, 240);
		contentPane.add(label);
	}
}
