import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

public class adm_choose extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					adm_choose frame = new adm_choose();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public adm_choose() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Documents and Settings\\Administrator\\Desktop\\bk pics\\java prjct\\icon.gif"));
		setBackground(Color.WHITE);
		setTitle("Library Management System Panel");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 448, 363);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton button = new JButton("ISSUE A BOOK !");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				issuebook ib=new issuebook();
	    		ib.setVisible(true);
	    		setVisible(false);
			}
		});
		
		JButton btnNewButton = new JButton("Insert Books In Library !");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				insertbooks ib= new insertbooks();
				ib.setVisible(true);
				setVisible(false);
			}
		});
		btnNewButton.setBounds(75, 240, 290, 34);
		contentPane.add(btnNewButton);
		
		JLabel lblForOfficialUse = new JLabel("For Official Use Only !");
		lblForOfficialUse.setFont(new Font("Times New Roman", Font.PLAIN, 11));
		lblForOfficialUse.setBounds(0, 315, 196, 14);
		contentPane.add(lblForOfficialUse);
		button.setBounds(75, 50, 135, 34);
		contentPane.add(button);
		
		JButton btnCreateStudentId = new JButton("CREATE STUDENT ID");
		btnCreateStudentId.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				StdMember sm=new StdMember();
	    		sm.setVisible(true);
	    		setVisible(false);
			}
		});
		btnCreateStudentId.setBounds(58, 111, 151, 34);
		contentPane.add(btnCreateStudentId);
		
		JButton button_2 = new JButton("UPDATE A RECORD !");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				update up=new update();
	    		up.setVisible(true);
	    		setVisible(false);
			}
		});
		button_2.setBounds(65, 174, 145, 34);
		contentPane.add(button_2);
		
		JButton button_3 = new JButton("SEARCH A RECORD !");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				search sc=new search();
	    		sc.setVisible(true);
	    		setVisible(false);
			}
		});
		button_3.setBounds(220, 174, 145, 34);
		contentPane.add(button_3);
		
		JButton button_4 = new JButton("RETURN A BOOK !");
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				user_returnbook rb=new user_returnbook();
	    		rb.setVisible(true);
	    		setVisible(false);
			}
		});
		button_4.setBounds(220, 111, 160, 34);
		contentPane.add(button_4);
		
		JButton button_5 = new JButton("DELETE A RECORD !");
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				delete d=new delete();
	    		d.setVisible(true);
	    		setVisible(false);
			}
		});
		button_5.setBounds(220, 50, 145, 34);
		contentPane.add(button_5);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\Documents and Settings\\Administrator\\Desktop\\Pics\\1731-books-library-wallpaper.jpg"));
		lblNewLabel.setBounds(0, 0, 440, 329);
		contentPane.add(lblNewLabel);
	}
}
