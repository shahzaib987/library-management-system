import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.*;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

public class delete extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField name;
	private JTextField phone;
	private JTextField class1;
	private JTextField city;
	private JTextField padd;
	private JTextField shift;
	private JTextField progress;
	private JTextField eadd;
	private JTextField id;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					delete frame = new delete();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public delete() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Documents and Settings\\Administrator\\Desktop\\bk pics\\java prjct\\icon.gif"));
		setTitle("Library Management System (Delete Panel)");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 777, 495);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Enter Your Username:");
		label.setForeground(Color.WHITE);
		label.setBounds(-63, -71, 108, 14);
		contentPane.add(label);
		
		name = new JTextField();
		name.setForeground(Color.BLACK);
		name.setBackground(Color.WHITE);
		name.setBounds(165, 167, 152, 26);
		contentPane.add(name);
		
		phone = new JTextField();
		phone.setForeground(Color.BLACK);
		phone.setBackground(Color.WHITE);
		phone.setBounds(516, 167, 152, 26);
		contentPane.add(phone);
		
		JLabel label_1 = new JLabel("Enter Phone Number :");
		label_1.setForeground(Color.WHITE);
		label_1.setBounds(356, 170, 150, 20);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("Student's Name :");
		label_2.setBackground(new Color(0, 0, 0));
		label_2.setForeground(Color.WHITE);
		label_2.setBounds(67, 170, 150, 20);
		contentPane.add(label_2);
		
		JLabel label_3 = new JLabel("Class");
		label_3.setBackground(new Color(0, 0, 0));
		label_3.setForeground(Color.WHITE);
		label_3.setBounds(67, 218, 150, 20);
		contentPane.add(label_3);
		
		class1 = new JTextField();
		class1.setForeground(Color.BLACK);
		class1.setBackground(Color.WHITE);
		class1.setBounds(165, 215, 152, 26);
		contentPane.add(class1);
		
		city = new JTextField();
		city.setForeground(Color.BLACK);
		city.setBackground(Color.WHITE);
		city.setBounds(516, 215, 152, 26);
		contentPane.add(city);
		
		padd = new JTextField();
		padd.setForeground(Color.BLACK);
		padd.setBackground(Color.WHITE);
		padd.setBounds(516, 257, 152, 26);
		contentPane.add(padd);
		
		JLabel label_4 = new JLabel("Permenent Address");
		label_4.setForeground(Color.WHITE);
		label_4.setBounds(356, 260, 150, 20);
		contentPane.add(label_4);
		
		JLabel label_5 = new JLabel("City");
		label_5.setForeground(Color.WHITE);
		label_5.setBounds(356, 218, 150, 20);
		contentPane.add(label_5);
		
		shift = new JTextField();
		shift.setForeground(Color.BLACK);
		shift.setBackground(Color.WHITE);
		shift.setBounds(165, 263, 152, 26);
		contentPane.add(shift);
		
		JLabel label_6 = new JLabel("Shift");
		label_6.setBackground(new Color(0, 0, 0));
		label_6.setForeground(Color.WHITE);
		label_6.setBounds(67, 266, 150, 20);
		contentPane.add(label_6);
		
		JLabel Progress = new JLabel("Progress");
		Progress.setBackground(new Color(0, 0, 0));
		Progress.setForeground(Color.WHITE);
		Progress.setBounds(67, 303, 150, 20);
		contentPane.add(Progress);
		
		progress = new JTextField();
		progress.setForeground(Color.BLACK);
		progress.setBackground(Color.WHITE);
		progress.setBounds(165, 300, 152, 26);
		contentPane.add(progress);
		
		eadd = new JTextField();
		eadd.setForeground(Color.BLACK);
		eadd.setBackground(Color.WHITE);
		eadd.setBounds(516, 297, 152, 26);
		contentPane.add(eadd);
		
		JLabel label_8 = new JLabel("Email Address");
		label_8.setForeground(Color.WHITE);
		label_8.setBounds(356, 300, 150, 20);
		contentPane.add(label_8);
		
		JButton btnDeleteARecord = new JButton("Delete a Record !");
		btnDeleteARecord.setIcon(new ImageIcon("C:\\Documents and Settings\\Administrator\\Desktop\\College Library Record Application\\remove.gif"));
		btnDeleteARecord.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try
				{
					Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
				    Connection c = DriverManager.getConnection("jdbc:odbc:Student");
				    Statement st = c.createStatement();
				    PreparedStatement ps=c.prepareStatement("Delete from StdLogin where userID="+id.getText());
				    JOptionPane.showMessageDialog(null,"Student Record Deleted !");
				    ps.executeUpdate();
					//JOptionPane.showMessageDialog(this,"Record Deleted Successfully","SUCCESS",JOptionPane.INFORMATION_MESSAGE);
					c.close();
					st.close();
				 }
				catch(Exception e1)
				{
					
				}
				}

});
		btnDeleteARecord.setBounds(324, 384, 162, 27);
		contentPane.add(btnDeleteARecord);
		
		JButton btnNewButton = new JButton("Search Record For a Student !");
		btnNewButton.setIcon(new ImageIcon("C:\\Documents and Settings\\Administrator\\Desktop\\College Library Record Application\\SEARCH.PNG"));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try
			    {
			       Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			       Connection c = DriverManager.getConnection("jdbc:odbc:Student");
			       Statement st = c.createStatement();
			       JOptionPane.showMessageDialog(null,"Student ID Created!");
			       ResultSet rs = st.executeQuery("select * from StdLogin where userID=" +id.getText());

			       while(rs.next())
			       {		  // date1.setText(rs.getString("DateToday"));	
				    	    //id.setText(rs.getString("Id"));
					    name.setText(rs.getString("FName"));
				    	padd.setText(rs.getString("Address"));
					    class1.setText(rs.getString("Class"));
					    progress.setText(rs.getString("Progress"));
					    phone.setText(rs.getString("PhoneNumber"));
					    shift.setText(rs.getString("Shift"));
					    city.setText(rs.getString("City"));
					   // padd.setText(rs.getString("Permenent_Address"));
					    eadd.setText(rs.getString("EmailAddress"));
					    
			      }
			        	c.close();
			        	st.close();
			    }
			        	catch(Exception e1)
						{
							
						}
						}
		
		});
		btnNewButton.setBounds(237, 42, 250, 26);
		contentPane.add(btnNewButton);
		
		id = new JTextField();
		id.setBounds(128, 45, 86, 20);
		contentPane.add(id);
		id.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Enter UserID:");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setBounds(28, 48, 78, 14);
		contentPane.add(lblNewLabel);
		
		JButton btnNewButton_1 = new JButton("Back To Options !");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				adm_choose ac=new adm_choose();
				ac.setVisible(true);
				setVisible(false);
			}
		});
		btnNewButton_1.setBounds(0, 0, 162, 23);
		contentPane.add(btnNewButton_1);
		
		JLabel label_7 = new JLabel("New label");
		label_7.setIcon(new ImageIcon("C:\\Documents and Settings\\Administrator\\Desktop\\Pics\\1731-books-library-wallpaper.jpg"));
		label_7.setBounds(0, 0, 770, 461);
		contentPane.add(label_7);
	}

}
