import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import java.awt.Color;

import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.ImageIcon;
import java.awt.Toolkit;


public class return_book extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable return_show;
	private JButton btnNewButton_1;
	private JButton btnBackToYour;
	private JLabel lblNewLabel;
	private JButton btnNewButton;
	private JLabel label;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					return_book frame = new return_book();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public return_book() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Documents and Settings\\Administrator\\Desktop\\bk pics\\java prjct\\icon.gif"));
		setResizable(false);
		setTitle("Return Books Panel !");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 659, 431);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnNewButton_1 = new JButton("Record For Borrowed Books !");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Object[][] s=new Object[1][06];
				try
				{
				    Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			    	Connection cn=DriverManager.getConnection("jdbc:odbc:Student");
			    	Statement stmt=cn.createStatement();
			    	 JOptionPane.showMessageDialog(null,"Your Borrowed Books !");
			    	ResultSet rs=stmt.executeQuery("select *  from  BookIssue where Username1='" +StdLogin.textField.getText()+  "'");
			    	//ResultSetMetaData rsmd = rs.getMetaData();
			    	//rsmd.getColumnName(1);
			    	//rsmd.getColumnName(2);
			    	//rsmd.getColumnName(3);
			    	//rsmd.getColumnName(4);
			    	//rsmd.getColumnName(5);
			    	//rsmd.getColumnName(6);
			    	//rsmd.getColumnName(7);
			    	//rsmd.getColumnName(8);
			    	//rsmd.getColumnName(9);
			    	//rsmd.getColumnName(10);
			    	//rsmd.getColumnName(11);
			    	//rsmd.getColumnName(12);
			    	
			    	int i=0;
			    	while(rs.next())
			    	{
			    		//s[i][0]=rs.getString(1);
						s[i][0]=rs.getString(2);
						s[i][1]=rs.getString(3);
						s[i][2]=rs.getString(4);
						s[i][3]=rs.getString(5);
						s[i][4]=rs.getString(6);
						s[i][5]=rs.getString(7);
						//s[i][6]=rs.getString(8);
						//s[i][7]=rs.getString(9);
						//s[i][8]=rs.getString(10);
						//s[i][9]=rs.getString(11);
						i++;
									
			    		
//.setText(rs.getString("Id"));
			    	}
			    	
				}
				catch (Exception a)
				{
					
					
				}
				
				
				
				
				String[] columns=new String[06];
				//columns[0]="userid";
				//columns[0]="BookCode";
				//columns[0]="FName";
				//columns[0]="Class";
				//columns[0]="Shift";
				//columns[0]="Progress";
				//columns[0]="Address";
				//columns[0]="PhoneNumber";
				//columns[0]="UserID";
				columns[0]="BookID";
				columns[1]="Issued Date";
				columns[2]="BookIssue 1";
				columns[3]="BookIssue 2";
				columns[4]="Return Date";
				columns[5]="Book Returned";
				//columns[6]="Address";
				//columns[7]="Phone Number";
				//columns[8]="Email Address";
				//columns[9]="City";
				//columns[11]="rttaioisd";
				
				//table.getColumnModel().getColumn(1);
				return_show.setModel(new DefaultTableModel(s,columns));
	//table.getModel().getColumnName(1);
			}
		});
		btnNewButton_1.setBounds(341, 114, 203, 39);
		contentPane.add(btnNewButton_1);
		
		btnBackToYour = new JButton("Back To Student Panel");
		btnBackToYour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			user_panel up=new user_panel();
			up.setVisible(true);
			setVisible(false);
			}
		});
		btnBackToYour.setBounds(0, 0, 182, 23);
		contentPane.add(btnBackToYour);
		
		JButton btnReturnBook = new JButton("Return The Book !");
		btnReturnBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Connection con=null;
				try {
				      Class.forName("sun.jdbc.odbc.JdbcOdbcDriver") ;

				// Connect with a full url string
				         con = DriverManager.getConnection("jdbc:odbc:Student");
				      //JOptionPane.showMessageDialog(null, "Database Connection Confirmed!");
				      	Statement stmt=con.createStatement();
				    	JOptionPane.showMessageDialog(null, "Successful , Thank you for returning Books !");
				      	ResultSet rs=stmt.executeQuery("UPDATE BookIssue SET BookReturned = 1 where Username1='" +StdLogin.textField.getText()+ "'");
				    	//ResultSet alr=stmt.executeQuery("select ReturnDate from user_inf where Username='" +StdLogin.textField.getText()+ "'");
				    	rs.next();
				      	con.close();
						
						   
			    } catch (Exception e) {
			     
			    }
			
			  }
		
		
	});
				      	btnReturnBook.setBounds(117, 114, 203, 39);
		contentPane.add(btnReturnBook);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 211, 653, 55);
		contentPane.add(scrollPane);
		
		
		return_show = new JTable();
		scrollPane.setViewportView(return_show);
		
		lblNewLabel = new JLabel("Note : \"0\" Means The Book Is Not Returned , And \"1\" Means That You Have Returned The Books !");
		lblNewLabel.setForeground(Color.RED);
		lblNewLabel.setBounds(36, 374, 574, 14);
		contentPane.add(lblNewLabel);
		
		btnNewButton = new JButton("Refresh The Record !");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				Object[][] s=new Object[1][06];
				try
				{
				    Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			    	Connection cn=DriverManager.getConnection("jdbc:odbc:Student");
			    	Statement stmt=cn.createStatement();
			    	 JOptionPane.showMessageDialog(null,"Check And See The Changes Of Your BookReturned Column (1= Book Is Returned , 0= Book Is Not Returned) !");
			    	ResultSet rs=stmt.executeQuery("select *  from  BookIssue where Username1='" +StdLogin.textField.getText()+  "'");
			    	//ResultSetMetaData rsmd = rs.getMetaData();
			    	//rsmd.getColumnName(1);
			    	//rsmd.getColumnName(2);
			    	//rsmd.getColumnName(3);
			    	//rsmd.getColumnName(4);
			    	//rsmd.getColumnName(5);
			    	//rsmd.getColumnName(6);
			    	//rsmd.getColumnName(7);
			    	//rsmd.getColumnName(8);
			    	//rsmd.getColumnName(9);
			    	//rsmd.getColumnName(10);
			    	//rsmd.getColumnName(11);
			    	//rsmd.getColumnName(12);
			    	
			    	int i=0;
			    	while(rs.next())
			    	{
			    		//s[i][0]=rs.getString(1);
						s[i][0]=rs.getString(2);
						s[i][1]=rs.getString(3);
						s[i][2]=rs.getString(4);
						s[i][3]=rs.getString(5);
						s[i][4]=rs.getString(6);
						s[i][5]=rs.getString(7);
						//s[i][6]=rs.getString(8);
						//s[i][7]=rs.getString(9);
						//s[i][8]=rs.getString(10);
						//s[i][9]=rs.getString(11);
						i++;
									
			    		
//.setText(rs.getString("Id"));
			    	}
			    	
				}
				catch (Exception a)
				{
					
					
				}
				
				
				
				
				String[] columns=new String[06];
				//columns[0]="userid";
				//columns[0]="BookCode";
				//columns[0]="FName";
				//columns[0]="Class";
				//columns[0]="Shift";
				//columns[0]="Progress";
				//columns[0]="Address";
				//columns[0]="PhoneNumber";
				//columns[0]="UserID";
				columns[0]="BookID";
				columns[1]="Issued Date";
				columns[2]="BookIssue 1";
				columns[3]="BookIssue 2";
				columns[4]="Return Date";
				columns[5]="Book Returned";
				//columns[6]="Address";
				//columns[7]="Phone Number";
				//columns[8]="Email Address";
				//columns[9]="City";
				//columns[11]="rttaioisd";
				
				//table.getColumnModel().getColumn(1);
				return_show.setModel(new DefaultTableModel(s,columns));
	//table.getModel().getColumnName(1);
			}
		});
		btnNewButton.setBounds(454, 277, 167, 31);
		contentPane.add(btnNewButton);
		
		label = new JLabel("New label");
		label.setIcon(new ImageIcon("C:\\Documents and Settings\\Administrator\\Desktop\\bk pics\\bk (1).jpg"));
		label.setBounds(0, 0, 653, 399);
		contentPane.add(label);
	}
}
